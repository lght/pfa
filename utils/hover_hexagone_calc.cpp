// C/C++ File
// AUTHOR:   kyndt
// MAIL:     clovis.kyndt@gmail.com
// FILE:     graphicClient/utils/hover_hexagone_calc.c
// ROLE:     TODO (some explanation)
// CREATED:  2014-01-01 15:01:28
// MODIFIED: 2014-01-08 14:04:26

bool hover_hexagone(int, int, int, bool, int, int);

#include <iostream>

bool hover_hexagone(int x_max_i, int y_max_i, int border_i,
                    bool rotate, int mouse_x_i, int mouse_y_i)
{
  double a, b;
  double x1, y1, x2, y2;

  double x_max    = static_cast<double>(x_max_i);
  double y_max    = static_cast<double>(y_max_i);
  double border   = static_cast<double>(border_i);
  double mouse_x  = static_cast<double>(mouse_x_i); 
  double mouse_y  = static_cast<double>(mouse_y_i);

  if (mouse_y > (y_max / 2))
    mouse_y = (y_max - 1) - mouse_y;
  if (mouse_x < (x_max / 2))
    mouse_x = ((x_max)/ 2) + (((x_max - 1)/ 2) - mouse_x);
  if (mouse_x >= (x_max - border) || mouse_y < border)
    return (false);
  mouse_y = -mouse_y;
  if (!rotate)
  {
    x1 = ((x_max - 1) / 2);
    y1 = -1 * (border);
    x2 = (y_max - border);
    y2 = -1 * ((x_max - (2 * border) + 2) / 4);
  }
  else
  {
    x1 = (y_max - border);
    y1 = -1 * ((x_max - 1) / 2);
    x2 = y_max - ((x_max - (2 * border) + 2) / 4);
    y2 = -1 * border;
  }
  a = (y1 - y2) / (x1 - x2);
  b = ((x1 * y2) - (x2 * y1)) / (x1 - x2);
  /*
    std::cout << "A(" << x1 << ";" << y1 << ")  B(" << x2 << ":" << y2 << ")" << std::endl;
    std::cout << "a = " << a << "; b = " << b << std::endl;
    std::cout << "y = ax + b = " << static_cast<int>(a * mouse_x + b) << " // my = " << mouse_y << " ; mx = " << mouse_x<< std::endl;
  */
  if (static_cast<int>(a * mouse_x + b) > mouse_y)
  {
    return (false);
  }
  return (true);
}
