-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 03 Février 2014 à 13:10
-- Version du serveur: 5.5.34
-- Version de PHP: 5.4.6-1ubuntu1.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `hexatyla_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `FriendList`
--

CREATE TABLE IF NOT EXISTS `FriendList` (
  `idFriendList` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idUser` int(10) unsigned NOT NULL,
  `idUserFriend1` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idFriendList`,`idUser`,`idUserFriend1`),
  KEY `fk_FriendList_User_idx` (`idUser`),
  KEY `fk_FriendList_User1_idx` (`idUserFriend1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `FriendRequest`
--

CREATE TABLE IF NOT EXISTS `FriendRequest` (
  `idRequest` int(11) NOT NULL AUTO_INCREMENT,
  `idAsk` int(11) unsigned NOT NULL,
  `idTarget` int(11) unsigned NOT NULL,
  PRIMARY KEY (`idRequest`),
  KEY `idAsk` (`idAsk`,`idTarget`),
  KEY `idTarget` (`idTarget`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `idUser` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `adresseMail` varchar(64) DEFAULT NULL,
  `raceXLost` int(10) unsigned DEFAULT NULL,
  `raceXWin` int(10) unsigned DEFAULT NULL,
  `raceYLost` int(10) unsigned DEFAULT NULL,
  `raceYWin` int(10) unsigned DEFAULT NULL,
  `raceZLost` int(10) unsigned DEFAULT NULL,
  `raceZWin` int(10) unsigned DEFAULT NULL,
  `inscription` date NOT NULL,
  `last_connect` date DEFAULT NULL,
  `avatar` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `FriendList`
--
ALTER TABLE `FriendList`
  ADD CONSTRAINT `fk_FriendList_User` FOREIGN KEY (`idUser`) REFERENCES `User` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_FriendList_User1` FOREIGN KEY (`idUserFriend1`) REFERENCES `User` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `FriendRequest`
--
ALTER TABLE `FriendRequest`
  ADD CONSTRAINT `FriendRequest_ibfk_1` FOREIGN KEY (`idAsk`) REFERENCES `User` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FriendRequest_ibfk_2` FOREIGN KEY (`idTarget`) REFERENCES `User` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
