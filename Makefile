##
## Makefile for  in /home/geoffrey/Projects/pfa
## 
## Made by geoffrey bauduin
## Login   <baudui_g@epitech.net>
## 
## Started on  Wed Feb  5 14:00:43 2014 geoffrey bauduin
## Last update Fri Mar 21 13:05:29 2014 gautier lefebvre
##

NAME_SERVER=		server

NAME_CLIENT=		libclient.a

NAME_TEST_CLIENT=	client

NAME_TEST=		test

NAME_BETA=		beta


SRCDIR=			src/
GUIDIR=			$(SRCDIR)GUI/
GUIGDIR=		$(SRCDIR)GUIG/
BETADIR=		$(SRCDIR)beta/
APPDIR=			$(SRCDIR)applications/
OVLDIR=			$(SRCDIR)overlays/
SCNDIR=			$(SRCDIR)scene/
UTILDIR=		$(SRCDIR)utils/
EFFDIR=			$(SRCDIR)effects/
ELEM3D=			$(SRCDIR)3DElements/

SRC_NETWORK=		src/Network/Socket.cpp			\
			src/Network/SSocket.cpp			\
			src/Network/Exception.cpp

SRC_NETWORK_CLIENT=	src/Network/Manager/Client.cpp

SRC_NETWORK_SERVER=	src/Network/UserJob.cpp			\
			src/Network/Manager/Server.cpp

SRC_THREADING=		src/Threading/Mutex.cpp			\
			src/Threading/Thread.cpp		\
			src/Threading/ScopeLock.cpp		\
			src/Threading/CondVar.cpp		\
			src/Threading/Pool.cpp

SRC_PROTOCOL=		src/Protocol/Error.cpp			\
			src/Protocol/Job.cpp			\
			src/Protocol/Color.cpp			\
			src/Protocol/Command.cpp		\
			src/Protocol/Difficulty.cpp		\
			src/Protocol/Race.cpp			\
			src/Protocol/User.cpp			\
			src/Protocol/JobResolver.cpp

MYSQL_SRC=		src/MySQL/Database.cpp			\
			src/MySQL/Row.cpp			\
			src/MySQL/Result.cpp			\
			src/MySQL/Exception.cpp			\
			src/MySQL/DBList.cpp

SRC_COMMON=		src/Date.cpp				\
			src/Logger.cpp				\
			src/Algo/MD5.cpp			\
			src/Clock.cpp				\
			src/HasSerial.cpp			\
			src/HasInfos.cpp			\
			src/HasID.cpp				\
			src/Factory/Factory.cpp			\
			src/Factory/Kernel.cpp			\
			src/Factory/Protocol.cpp		\
			src/Factory/Game.cpp			\
			src/Utils/FileReader.cpp		\
			src/Parser/JSON.cpp			\
			src/Parser/SQL.cpp			\
			src/Kernel/Data.cpp			\
			src/Kernel/Cost.cpp			\
			src/Kernel/ElementRessources.cpp	\
			src/Kernel/Time.cpp			\
			src/Kernel/Manager.cpp			\
			src/Kernel/CapacityList.cpp		\
			src/Kernel/Serial.cpp			\
			src/Kernel/CapacityData.cpp		\
			src/Kernel/BuildingData.cpp		\
			src/Kernel/ID.cpp			\
			src/Kernel/Effect.cpp			\
			src/Game/Ressources.cpp			\
			src/Game/Position.cpp			\
			src/Game/Movable.cpp			\
			src/Game/Amelioration.cpp		\
			src/Game/AElement.cpp			\
			src/Game/Building.cpp			\
			src/Game/Hero.cpp			\
			src/Game/Unit.cpp			\
			src/Game/Player.cpp			\
			src/Game/Race.cpp			\
			src/Game/Projectile.cpp			\
			src/Game/Requirements.cpp		\
			src/Game/Capacity.cpp			\
			src/Game/Team.cpp			\
			src/Game/Effect.cpp			\
			src/Game/Map.cpp			\
			src/Game/Controller.cpp			\
			src/Game/Object.cpp			\
			src/Game/RessourcesSpot.cpp		\
			src/Game/ProductionQueue.cpp		\
			src/Game/ABase.cpp			\
			src/Game/Action.cpp			\
			src/Game/CanUseCapacity.cpp		\
			src/Parser/ARace.cpp			\
			src/Parser/Map.cpp			\
			src/FileLoader.cpp			\
			src/Utils/Hexagon.cpp			\
			src/Kernel/Config.cpp			\
			src/NoDeleteWhileUsedByAThread.cpp

SRC_SERVER=		$(SRC_COMMON)				\
			$(MYSQL_SRC)				\
			$(SRC_NETWORK)				\
			$(SRC_NETWORK_SERVER)			\
			$(SRC_THREADING)			\
			$(SRC_PROTOCOL)				\
			src/Server/Group.cpp			\
			src/Server/MatchMaking.cpp		\
			src/Factory/Server.cpp			\
			src/Factory/Network.cpp			\
			src/Algo/Pathfinding.cpp		\
			src/Room/MapList.cpp			\
			src/Room/MapInfos.cpp			\
			src/Room/Type.cpp			\
			src/Room/Settings.cpp			\
			src/Room/ARoom.cpp			\
			src/Room/AQuickLaunch.cpp		\
			src/Room/OneVsOne.cpp			\
			src/Room/TwoVsTwo.cpp			\
			src/Room/ThreeVsThree.cpp		\
			src/Server/main.cpp			\
			src/Server/Core.cpp			\
			src/Server/AItem.cpp			\
			src/Server/Event.cpp			\
			src/Server/HasEvent.cpp			\
			src/Server/HasUsers.cpp			\
			src/Server/Building.cpp			\
			src/Server/Unit.cpp			\
			src/Server/Hero.cpp			\
			src/Server/Projectile.cpp		\
			src/Server/Game.cpp			\
			src/Server/HasUnit.cpp			\
			src/Server/HasProjectile.cpp		\
			src/Server/HasBuilding.cpp		\
			src/Server/HasHero.cpp			\
			src/Server/HasObject.cpp		\
			src/Server/User.cpp			\
			src/Server/HasJobs.cpp			\
			src/Server/GamePlayer.cpp		\
			src/Server/HasProductionQueue.cpp	\
			src/Server/Object.cpp			\
			src/Server/RessourcesSpot.cpp		\
			src/Server/HasRessourcesSpot.cpp	\
			src/Server/Effect.cpp			\
			src/Server/Capacity.cpp			\
			src/Server/Controller.cpp		\
			src/Server/HasStatistics.cpp		\
			src/Server/GameLoader.cpp		\
			src/Server/Movable.cpp			\
			src/Server/HasPlayers.cpp		\
			src/Server/Map.cpp			\
			src/Server/Action.cpp			\
			src/Server/Waitlist.cpp			\
			src/Server/CanUseCapacity.cpp		\
			src/Server/CanBeDamaged.cpp		\
			src/Server/GroupMove.cpp		\
			src/Server/Spectator.cpp		\
			src/Parser/Race.cpp			\
			src/Server/Race.cpp

SRC_CLIENT=		$(SRC_NETWORK)				\
			$(SRC_NETWORK_CLIENT)			\
			$(SRC_PROTOCOL)				\
			$(SRC_COMMON)				\
			$(SRC_THREADING)			\
			src/Factory/Client.cpp			\
			src/Parser/RaceClient.cpp

SRC_BETA=		$(GUIDIR)Controller.cpp				\
			$(GUIDIR)Convers.cpp				\
			$(GUIDIR)Core.cpp				\
			$(GUIDIR)Friends.cpp				\
			$(GUIDIR)GameLoading.cpp			\
			$(GUIDIR)Group.cpp				\
			$(GUIDIR)Popup.cpp				\
			$(GUIDIR)Profile.cpp				\
			$(GUIDIR)Race.cpp				\
			$(GUIDIR)Setting.cpp				\
			$(GUIDIR)SignInUp.cpp				\
			$(GUIDIR)Tooltip.cpp				\
			$(GUIGDIR)AGUIG.cpp				\
			$(GUIGDIR)Profile.cpp				\
			$(GUIGDIR)Ressource.cpp				\
			$(GUIGDIR)ChatBox.cpp				\
			$(GUIGDIR)MiniMap.cpp				\
			$(GUIGDIR)DayNight.cpp				\
			$(GUIGDIR)Menu.cpp				\
			$(GUIGDIR)Controller.cpp			\
			$(GUIGDIR)Progress.cpp				\
			$(GUIGDIR)Annotation.cpp			\
			$(GUIGDIR)AnnotationBis.cpp			\
			$(GUIGDIR)AllyMenu.cpp				\
			$(GUIGDIR)Squad.cpp				\
			$(SRCDIR)main.cpp				\
			$(UTILDIR)graphicException.cpp			\
			$(APPDIR)Application.cpp			\
			$(EFFDIR)MoveObject.cpp				\
			$(EFFDIR)MoveOverlay.cpp			\
			$(EFFDIR)AEffect.cpp				\
			$(EFFDIR)ARepeatable.cpp			\
			$(EFFDIR)AMove.cpp				\
			$(EFFDIR)AFade.cpp				\
			$(EFFDIR)Fade.cpp				\
			$(EFFDIR)AScale.cpp				\
			$(EFFDIR)Scale.cpp				\
			$(SCNDIR)AScene.cpp				\
			$(SCNDIR)MenuScene.cpp				\
			$(OVLDIR)AOverlay.cpp				\
			$(OVLDIR)OverlayImage.cpp			\
			$(OVLDIR)OverlayText.cpp			\
			$(OVLDIR)TextBox.cpp				\
			$(OVLDIR)InputText.cpp				\
			$(OVLDIR)Button.cpp				\
			$(OVLDIR)Click.cpp				\
			$(OVLDIR)AOverlaySceneManager.cpp		\
			$(OVLDIR)MouseOverlayManager.cpp		\
			$(OVLDIR)MainMenuOverlaySceneManager.cpp	\
			$(ELEM3D)Element3DContainer.cpp			\
			$(ELEM3D)Element3DPoolManager.cpp

SRC_TEST_CLIENT=	src/Client/JobController.cpp			\
			$(SRC_CLIENT)					\
			$(SRC_BETA)

OBJ_BETA=		$(SRC_BETA:.cpp=.o)

OBJ_SERVER=		$(SRC_SERVER:.cpp=.o)

OBJ_TEST_CLIENT=	$(SRC_TEST_CLIENT:.cpp=.oc)

DEPS_SERVER =		$(OBJ_SERVER:.o=.d)
DEPS_CLIENT =		$(OBJ_TEST_CLIENT:.oc=.dc)

LCEGUI=		-L ./libs -L ./libs/cegui -l CEGUIBase-0 -lCEGUIOgreRenderer-0
ICEGUI=		-I./includes/Libs

LOGRE=		-L ./libs -L ./libs/OGRE -lOgreMain -lOgreOverlay
IOGRE=		-I./includes/Libs/Ogre -I./includes/Libs/Ogre/Overlay

LOIS=		-L ./libs -lOIS
IOIS=		-I./includes/Libs/OIS/

IMY=		-I./includes/ -I./includes/Game -I./../includes/

LDFLAGS=	$(LOGRE) $(LOIS) -lboost_system -Wl,-rpath,./libs/ -Wl,-rpath,./libs/OGRE -lssl -lcrypto -lpthread

LDFLAGS_SERVER=	$(LDFLAGS) -L/usr/include/mysql -lmysqlclient -I/usr/include/mysql -Llib/ -ljson_linux-gcc-4.8_libmt -Wl,-rpath,./lib/ -lm

LDFLAGS_CLIENT=	$(LDFLAGS) -Llib/ -ljson_linux-gcc-4.8_libmt -Wl,-rpath,./lib/

INCLUDES=	$(IMY) $(IOGRE) $(IOIS) $(ICEGUI)

CXXFLAGS=	$(INCLUDES) -Iincludes/ -std=c++11 -g -ggdb

CXX=		g++

GREEN=       		\033[0;32m
YELLOW=			\033[0;36m
BLUE=			\033[0;34m
NOCOLOR=	       	\033[0m
ORANGE=			\033[0;35m
RED=			\033[0;31m

all:		$(NAME_SERVER) $(NAME_TEST_CLIENT)

$(NAME_SERVER): CXXFLAGS += -D__SERVER__
$(NAME_SERVER):	CXXFLAGS += -Wall -Wextra
$(NAME_SERVER):	$(OBJ_SERVER)
		@$(CXX) -o $(NAME_SERVER) $(OBJ_SERVER) $(LDFLAGS_SERVER) && echo "$(BLUE)Building successful$(NOCOLOR)\t[$(NAME_SERVER)]"

##  g++ -c -MMD src/Clock.cpp -MF depend -o Clock.o -Iincludes/ -std=c++11

%.oc:		%.cpp
		@$(CXX) -c $(CXXFLAGS) -MMD $< -o $@ -MF $(patsubst %.oc, %.dc, $@) && echo "$(GREEN)compiled$(NOCOLOR)\t[$<] $(GREEN)>>$(NOCOLOR) [$@]"

%.o:		%.cpp
		@$(CXX) -c $(CXXFLAGS) -MMD $< -o $@ -MF $(patsubst %.o, %.d, $@) && echo "$(YELLOW)compiled$(NOCOLOR)\t[$<] $(YELLOW)>>$(NOCOLOR) [$@]"

$(NAME_TEST_CLIENT):	CXXFLAGS += -DCLIENT__
$(NAME_TEST_CLIENT):	$(OBJ_TEST_CLIENT)
			@$(CXX) -o $(NAME_TEST_CLIENT) $(OBJ_TEST_CLIENT) $(LDFLAGS_CLIENT) && echo "$(BLUE)Building successful$(NOCOLOR)\t[$(NAME_TEST_CLIENT)]"

clean:		clean_client clean_server

clean_client:
		@rm -f $(OBJ_TEST_CLIENT) && echo "Objects removed."

fclean_server:	clean_server
		@rm -f $(NAME_SERVER) && echo "Removed [$(NAME_SERVER)]"

clean_server:
		@rm -f $(OBJ_SERVER) && echo "Objects removed."

fclean_client:	clean_client
		@rm -f $(NAME_TEST_CLIENT) && echo "Removed [$(NAME_TEST_CLIENT)]"

fclean:		fclean_server fclean_client

hardclean:	fclean
		@rm -f $(DEPS_SERVER) $(DEPS_CLIENT)

re:		fclean all

debug:		CXXFLAGS += -DMDEBUG
debug:		all

opti:		CXXFLAGS += -Og
opti:		all

-include $(DEPS_SERVER)
-include $(DEPS_CLIENT)
