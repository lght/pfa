#!/bin/sh

if [ $# -eq 2 ]; then
    mv "$1" "`sha1sum $1 | cut -d' ' -f1`_$2.hxtl"
fi